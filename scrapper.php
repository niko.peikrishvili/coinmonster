<?php

$startTime = time();
$startDate = date('Y-m-d H:i:s');
require_once __DIR__ . '/vendor/autoload.php';

use CoinMonster\CoinMarketCup;
use CoinMonster\DataBase;
use CoinMonster\CoinMarketCupGrabber;

$grabber = new CoinMarketCupGrabber();
$cron = new CoinMarketCup();

$report = "";
$i = 0;
$totaltime = 0;
try
{
    $database = new DataBase($cron->getDatabaseSettings());
    $data = $database->getAll();

    foreach ($data as $item)
    {
        $localTime = time();
        $result = $grabber->returnInfoArray($item['Website_Slug']);
        $result['icon'] = $grabber->saveIcon($item);
        $result['logo'] = $grabber->saveLogo($item);
        $result['image'] = $grabber->saveImage($item);
        $result['description'] = $grabber->getDescription($item['Website_Slug']);
        $database->updateScrapperInfo($item['ID'], $result);
        $localtimespend = (time() - $localTime);
        echo $grabber->returnDebugString($item, $result, $localtimespend, $i);
        $totaltime += $localtimespend;
        $i++;
    }
    echo "[OK] Total time spent on execution : " . $totaltime . "\n";
} catch (\Exception $ex)
{
    echo "[WARNING] Total time spent on execution : " . $totaltime . "\n";
}
