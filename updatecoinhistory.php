<?php

$startTime = time();
$startDate = date('Y-m-d H:i:s');
require_once __DIR__ . '/vendor/autoload.php';

use CoinMonster\CoinMarketCup;
use CoinMonster\DataBase;
use CoinMonster\Helper;

$cron = new CoinMarketCup();
$cron->parseDelay($argv);
$report = "";
try
{
    $database = new DataBase();
    $urls = $cron->returnUrls();
    $founded = 0;
    $notfounded = 0;
    $urlused = 0;
    echo str_pad("DATE", 21, "=", STR_PAD_BOTH) . " | ";
    echo str_pad("APIURL", 86, "=", STR_PAD_BOTH) . " | ";
    echo str_pad("STATISTIC", 43, "=", STR_PAD_BOTH) . " | ";

    echo "\n";
    echo str_pad("", 160, "-", STR_PAD_BOTH) . "\n";
    foreach ($urls as $url)
    {
        $requestStartTime = time();
        $ids = '';
        $data = Helper::getData($url, $cron->getDelay())->data;
        $urlwasused = false;
        foreach ($data as $item)
        {
            $hightLow = $database->get24HourMinMaxForCrypto($item->id);
            $database->update24HourMinMax($item->id, $hightLow['low'], $hightLow['high']);
            if ($cron->isAllowed($item->id))
            {
                $ids .= $item->id . ',';
                $founded++;
                $database->addToHistory($item);
                $urlwasused = true;
            } else
            {
                $notfounded++;
            }
        }
        if ($urlwasused)
        {
            ++$urlused;
        }

        $requestEndTime = time();

        echo "[" . str_pad(date('Y-m-d H:i:s'), 18) . "] | ";
        echo "API : " . str_pad($url, 80) . " | ";
        echo "Time spend on request : " . str_pad(($requestEndTime - $requestStartTime), 3) . " seconds";
        echo "| " . $ids . "\n";
    }

    $endTime = time();
    $endDate = date('Y-m-d H:i:s');
    $totaltime = $endTime - $startTime;
    echo "=================REPORT=========================\n";
    echo "Founded ids : " . $founded . " from " . count($cron->getUpdatableIds()) . "\n";
    echo "Not founded ids : " . $notfounded . "\n";
    echo "Used API URL " . $urlused . " from " . count($urls) . "\n";
    echo "Unused API URL " . (count($urls) - $urlused) . " from " . count($urls) . "\n";
    echo "Total Execution time : " . $totaltime . "\n";
    echo "Status : OK\n";
} catch (\Exception $ex)
{
    echo "=================REPORT=========================\n";
    $endTime = time();
    $endDate = date('Y-m-d H:i:s');
    $totaltime = $endTime - $startTime;
    echo "Total Execution time : " . $totaltime . "\n";
    echo "Status : Error : " . $ex->getMessage() . "\n";
}
echo "=================END=========================\n";
