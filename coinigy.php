<?php

$startTime = time();
$startDate = date('Y-m-d H:i:s');
require_once __DIR__ . '/vendor/autoload.php';

use CoinMonster\CoinIgyHistory;
use CoinMonster\DataBase;


try
{
    $coinigy = new CoinIgyHistory();
    $database = new DataBase();
    $result = $coinigy->getResponseFromMarketTickerApi();

    foreach ($result as $item)
    {
        if ($coinigy->isSaveAllowed($item))
        {
            $changes = $database->getCoinigyChanges($item);
            $onehour = $database->getMinMaxForCoinigyByHours($item, 1);
            $twentyfourhour = $database->getMinMaxForCoinigyByHours($item, 24);
            $sevendays = $database->getMinMaxForCoinigyByHours($item, (24*7));
            // append change and percent change
            $item->change = $changes->change;
            $item->changepercent = $changes->changepercent;
            // append 1 hour low and high
            $item->oneHLow = $onehour->low;
            $item->oneHHigh = $onehour->high;
            // append 24 hour low and high
            $item->twentyFourHLow = $twentyfourhour->low;
            $item->twentyFourHHigh = $twentyfourhour->high;
            // append 7 days low and high
            $item->sevenDayLow = $sevendays->low;
            $item->sevenDayHigh = $sevendays->high;
            $database->insertCoinigyHistory($item);
        }
    }
} catch (Exception $ex)
{
    
}
