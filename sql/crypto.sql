-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 19, 2018 at 07:38 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crypto`
--

-- --------------------------------------------------------

--
-- Table structure for table `coinhistory`
--

CREATE TABLE `coinhistory` (
  `CoinHistory_Index` int(11) NOT NULL,
  `Source` text NOT NULL,
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Symbol` text NOT NULL,
  `Price` decimal(30,9) NOT NULL,
  `Last_Updated_On` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coinmaster`
--

CREATE TABLE `coinmaster` (
  `Coin_Index` int(11) NOT NULL,
  `Source` longtext,
  `ID` int(11) DEFAULT NULL,
  `Name` longtext,
  `Symbol` longtext,
  `Website_Slug` longtext,
  `Rank` int(11) DEFAULT NULL,
  `Circulating_Supply` decimal(30,9) DEFAULT NULL,
  `Total_Supply` decimal(30,9) DEFAULT NULL,
  `Max_Supply` decimal(30,9) DEFAULT NULL,
  `Last_Price` decimal(30,9) DEFAULT NULL,
  `Volume_24h` decimal(30,9) DEFAULT NULL,
  `Market_Cap` decimal(30,9) DEFAULT NULL,
  `Percent_Change_1h` decimal(30,9) DEFAULT NULL,
  `Percent_Change_24h` decimal(30,9) DEFAULT NULL,
  `Percent_Change_7d` decimal(30,9) DEFAULT NULL,
  `Last_Updated_On` timestamp NULL DEFAULT NULL,
  `Description` longtext,
  `Website` longtext,
  `Icon` longtext,
  `Logo` longtext,
  `Image` longtext,
  `Twitter` longtext,
  `Telegram` longtext,
  `Facebook` longtext,
  `Github` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coinhistory`
--
ALTER TABLE `coinhistory`
  ADD PRIMARY KEY (`CoinHistory_Index`);

--
-- Indexes for table `coinmaster`
--
ALTER TABLE `coinmaster`
  ADD PRIMARY KEY (`Coin_Index`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coinhistory`
--
ALTER TABLE `coinhistory`
  MODIFY `CoinHistory_Index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `coinmaster`
--
ALTER TABLE `coinmaster`
  MODIFY `Coin_Index` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
