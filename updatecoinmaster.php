<?php

$startTime = time();
$startDate = date('Y-m-d H:i:s');
require_once __DIR__ . '/vendor/autoload.php';

use CoinMonster\CoinMarketCup;
use CoinMonster\DataBase;
use CoinMonster\Helper;
$cron = new CoinMarketCup();
$ids = '';
$report = "";
try
{
    echo "==================START========================\n";
    $cron->parseArgs($argv);
    $database = new DataBase();
    $database->getAllIds();
    $data = Helper::getData($cron->getUrl($cron->from, $cron->to),$cron->getDelay());

    foreach ($data->data as $item)
    {
        $requestStartTime = time();
        if ($database->needToInsert($item->id))
        {
            $database->addToCoinMaster($item);
        } else
        {
            $database->updateCoinMaster($item);
        }
    }
    $requestEndTime = time();
    echo "[" . date('Y-m-d H:i:s') . "] | ";
    echo "API : " . $cron->getUrl($cron->from, $cron->to) . " | ";
    echo "Time spend on request : " . ($requestEndTime - $requestStartTime) . " | \n";
    echo "=================REPORT=========================\n";

    $endTime = time();
    $endDate = date('Y-m-d H:i:s');
    $totaltime = $endTime - $startTime;
    echo "Total Execution time : " . $totaltime . "\n";
    echo "Status : OK\n";
} catch (\Exception $ex)
{
    echo "=================REPORT=========================\n";

    $endTime = time();
    $endDate = date('Y-m-d H:i:s');
    $totaltime = $endTime - $startTime;
    echo "Total Execution time : " . $totaltime . "\n";
    echo "Status : Error : " . $ex->getMessage() . "\n";
}
echo "=================END=========================\n";
