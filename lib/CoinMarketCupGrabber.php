<?php

namespace CoinMonster;

/**
 * Description of CoinMarketCupGrabber
 *
 * @author niko.peikrishvili
 */
class CoinMarketCupGrabber
{

    private $settingsFile = '/../data/image-settings.json';
    private $imageFolder;
    private $infoUrl = "https://coinmarketcap.com/currencies/%s/";
    private $iconUrl = "https://s2.coinmarketcap.com/static/img/coins/16x16/%d.png";
    private $logoUrl = "https://s2.coinmarketcap.com/static/img/coins/64x64/%d.png";
    private $imageUrl = "https://s2.coinmarketcap.com/static/img/coins/128x128/%d.png";
    private $coininfoUrl = "https://getcrypto.info/%s/";

    

    public function getImageFolder()
    {
        if (!is_array($this->imageFolder))
        {
            $this->updatableIds = Helper::checkFileAndReturnObject(__DIR__ . $this->settingsFile)->image_folder;
        }
        return $this->updatableIds;
    }

    protected function getInfoUrl($slug)
    {
        return sprintf($this->infoUrl, $slug);
    }

    protected function getDescriptionUrl($slug)
    {
        return sprintf($this->coininfoUrl, $slug);
    }

    public function returnInfoArray($slug)
    {
        $array = ['explorer'=>'','source code'=>'','website'=>'','twitter'=>''];
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $this->getInfoUrl($slug));
        if ($response->getStatusCode() != 200)
        {
            return [];
        }
        $matches = [];
        $content = $response->getBody()->getContents();
        $array['twitter'] = $this->getTwitter($content);
        
        $str = str_replace("\n", "", $content);
        $re = '/<div class="row bottom-margin-2x">.*<\/span><\/li><\/ul><\/div><\/div>/i';
        preg_match_all($re, $str, $matches);
        $liMatches = [];
        $re2 = '/<ul class="list-unstyled">.*<\/ul>/i';
        preg_match_all($re2, $matches[0][0], $liMatches);

        $xmlObject = simplexml_load_string($liMatches[0][0]);
        if (is_object($xmlObject) && property_exists($xmlObject, 'li') && count($xmlObject->li) > 0)
        {
            foreach ($xmlObject->li as $li)
            {
                if (trim($li->a[0]) != "")
                {
                    $array[strtolower($li->a[0])] = (string) $li->a["href"];
                }
            }
        }
        return $array;
    }

    public function getTwitter($src)
    {
        $twitterMatch = [];
        $re2 = '/<a class="twitter-timeline" .*<\/a>/i';
        $twitter = "";
        if(preg_match($re2, $src, $twitterMatch)===1)
        {
            $twitter =  str_replace("Tweets by ","",strip_tags($twitterMatch[0]));
        }
        return $twitter;
    }
    public function getFile($url)
    {
        try
        {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $url);
            if ($response->getStatusCode() != 200)
            {
                return false;
            }
            return $response->getBody()->getContents();
        } catch (\Exception $ex)
        {
            return false;
        }
    }

    public function saveIcon($item)
    {
        if (!file_exists($this->getImageFolder() . '/html/icon/'))
        {
            mkdir($this->getImageFolder() . '/html/icon/');
        }
        $filesource = $this->getFile(sprintf($this->iconUrl, $item['ID']));
        $filename = $this->getImageFolder() . '/html/icon/' . $item['ID'] . '-' . $item['Website_Slug'] . '-icon.png';
        if (file_put_contents($filename, $filesource))
        {
            return $filename;
        }
        return false;
    }

    public function saveLogo($item)
    {
        if (!file_exists($this->getImageFolder() . '/html/logo/'))
        {
            mkdir($this->getImageFolder() . '/html/logo/');
        }
        $filesource = $this->getFile(sprintf($this->logoUrl, $item['ID']));
        $filename = $this->getImageFolder() . '/html/logo/' . $item['ID'] . '-' . $item['Website_Slug'] . '-logo.png';
        if (file_put_contents($filename, $filesource))
        {
            return $filename;
        }
        return false;
    }

    public function saveImage($item)
    {
        if (!file_exists($this->getImageFolder() . '/html/image/'))
        {
            mkdir($this->getImageFolder() . '/html/image/');
        }
        $filesource = $this->getFile(sprintf($this->imageUrl, $item['ID']));
        $filename = $this->getImageFolder() . '/html/image/' . $item['ID'] . '-' . $item['Website_Slug'] . '-image.png';
        if (file_put_contents($filename, $filesource))
        {
            return $filename;
        }
        return false;
    }

    public function getDescription($slug)
    {
        $data = $this->getFile($this->getDescriptionUrl($slug));
        $data = trim(preg_replace('/\n/', '', $data));
        if ($data)
        {
            $matches = [];
            preg_match_all('/\<div class="faq">.*\<\!\-\- what are the coins \-\-\>/', $data, $matches);
            $textMatches = [];
            preg_match_all('/<p>.*<\/p>/', (string) $matches['0']['0'], $textMatches);
            return strip_tags($textMatches['0']['0']);
        }
        return false;
    }

    public function returnDebugString($item, $result, $time, $i)
    {
        if ($i == 0)
        {
            $header =  str_pad("ID", 4, "=", STR_PAD_BOTH)
            . " | " . str_pad("NAME", 30, "=", STR_PAD_BOTH)
            . " | " . str_pad("SITE", 5, "=", STR_PAD_BOTH)
            . " | " . str_pad("EXPLORER", 11, "=", STR_PAD_BOTH)
            . " | " . str_pad("SOURCE-CODE", 15, "=", STR_PAD_BOTH)
            . " | " . str_pad("TWITTER", 9, "=", STR_PAD_BOTH)
            . " | " . str_pad("ICON", 6, "=", STR_PAD_BOTH)
            . " | " . str_pad("LOGO", 6, "=", STR_PAD_BOTH)
            . " | " . str_pad("IMAGE", 7, "=", STR_PAD_BOTH)
            . " | " . str_pad("DESCRIPTION", 15, "=", STR_PAD_BOTH)
            . " | " . str_pad("TIME", 6, "=", STR_PAD_BOTH) . "\n";
            echo $header;
            echo str_pad("", strlen($header), "-") . "\n";
        }
        $str = "";
        $str .= str_pad($item['ID'], 4, " ", STR_PAD_BOTH) . " | ";
        $str .= str_pad($item['Website_Slug'], 30, " ", STR_PAD_RIGHT) . " | ";
        if (key_exists('website', $result) && $result['website'] != '')
        {
            $str .= str_pad("YES", 6, " ", STR_PAD_RIGHT) . " | ";
        } else
        {
            $str .= str_pad("NO", 6, " ", STR_PAD_RIGHT) . " | ";
        }
        if (key_exists('explorer', $result) && $result['explorer'] != '')
        {
            $str .= str_pad("YES", 11, " ", STR_PAD_RIGHT) . " | ";
        } else
        {
            $str .= str_pad("NO", 11, " ", STR_PAD_RIGHT) . " | ";
        }
        if (key_exists('source code', $result) && $result['source code'] != '')
        {
            $str .= str_pad("YES", 15, " ", STR_PAD_RIGHT) . " | ";
        } else
        {
            $str .= str_pad("NO", 15, " ", STR_PAD_RIGHT) . " | ";
        }
        if (key_exists('twitter', $result) && $result['twitter'] != '')
        {
            $str .= str_pad("YES", 15, " ", STR_PAD_RIGHT) . " | ";
        } else
        {
            $str .= str_pad("NO", 15, " ", STR_PAD_RIGHT) . " | ";
        }
        if (key_exists('icon', $result) && $result['icon'] != '')
        {
            $str .= str_pad("YES", 6, " ", STR_PAD_RIGHT) . " | ";
        } else
        {
            $str .= str_pad("NO", 6, " ", STR_PAD_RIGHT) . " | ";
        }
        if (key_exists('logo', $result) && $result['logo'] != '')
        {
            $str .= str_pad("YES", 6, " ", STR_PAD_RIGHT) . " | ";
        } else
        {
            $str .= str_pad("NO", 6, " ", STR_PAD_RIGHT) . " | ";
        }
        if (key_exists('image', $result) && $result['image'] != '')
        {
            $str .= str_pad("YES", 6, " ", STR_PAD_RIGHT) . " | ";
        } else
        {
            $str .= str_pad("NO", 6, " ", STR_PAD_RIGHT) . " | ";
        }
        if (key_exists('description', $result) && $result['description'] != "")
        {
            $str .= str_pad("YES", 15, " ", STR_PAD_RIGHT) . " | ";
        } else
        {
            $str .= str_pad("NO", 15, " ", STR_PAD_RIGHT) . " | ";
        }
        $str .= str_pad($time, 6, " ", STR_PAD_RIGHT);
        echo $str . "\n";
    }

}
