<?php

namespace CoinMonster;

/**
 * Description of Helper
 *
 * @author niko.peikrishvili
 */
class Helper
{
    /**
     * Check if file exist, parse json and if valid return object or array
     * @param type $file
     * @return type
     * @throws Exception
     */
    public static function checkFileAndReturnObject($file) {
        if (!file_exists($file)) {
            throw new Exception("File " . $file . " doesnot exists");
        }
        $object = json_decode(file_get_contents($file));
        if (!$object) {
            throw new Exception("File " . $file . " is not valid JSON file");
        }
        return $object;
    }
    
    /**
     * 
     * @param type $url
     * @param type $delay
     * @return \stdClass
     * @throws Exception
     */
    public function getData($url,$delay) {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);
        if ($response->getStatusCode() != 200) {
            throw new Exception("Error while connecting to " . $url . " endpoint", $response->getStatusCode());
        }
        $responseObject = json_decode($response->getBody()->getContents());
        if (!$responseObject || (!$responseObject instanceof \stdClass && !is_array($responseObject))) {
            throw new Exception("Cannot parse json from response");
        }
        sleep($delay);
        return $responseObject;
    }
}
