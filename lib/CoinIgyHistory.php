<?php

namespace CoinMonster;
use \Exception;
/**
 * Description of CoinIgyHistory
 *
 * @author niko.peikrishvili
 */
class CoinIgyHistory
{

    protected $settingsFile = '/../data/coinigyhistory-settings.json';
    protected $marketApiTickerUrl = "https://api.coinigy.com/api/v2/private/markets/ticker";
    protected $settings;

    public function getSettings()
    {
        if (!$this->settings)
        {
            $this->settings = json_decode(file_get_contents(__DIR__ . $this->settingsFile));
            if (!$this->settings)
            {
                throw new Exception("Unable parse JSON File");
            }
        }
        return $this->settings;
    }

    private function getKey()
    {
        if(!$this->getSettings() || !property_exists($this->getSettings(), "key") || empty($this->getSettings()->key))
        {
            throw new Exception("Missing api key for coinigy");
        }
        return $this->getSettings()->key;
    }

    private function getSecret()
    {
        if(!$this->getSettings() || !property_exists($this->getSettings(), "secret") || empty($this->getSettings()->secret))
        {
            throw new Exception("Missing api ssecret for coinigy");
        }
        return $this->getSettings()->secret;
    }

    public function getResponseFromMarketTickerApi()
    {
        $client = new \GuzzleHttp\Client(['headers' => [
                'X-API-KEY' => $this->getKey(),
                'X-API-SECRET' => $this->getSecret(),
            ],'debug'=>true]);
        $response = $client->request('POST', $this->marketApiTickerUrl);
        if ($response->getStatusCode() != 200)
        {
            throw new Exception("Error while connecting to " . $this->marketApiTickerUrl . " endpoint", $response->getStatusCode());
        }
        $responseObject = json_decode($response->getBody()->getContents());
        if (!$responseObject || (!$responseObject instanceof \stdClass && !is_array($responseObject)))
        {
            throw new Exception("Cannot parse json from response");
        }
        return $responseObject;
    }

    public function isSaveAllowed($item)
    {
        
    }

}
