<?php

namespace CoinMonster;

use \InvalidArgumentException;
use \Exception;

class CoinMarketCup {

    public $from;
    public $to;
    protected $url = 'https://api.coinmarketcap.com/v2/ticker/?start=%d&limit=%d&sort=id';
    protected $byIdurl = 'https://api.coinmarketcap.com/v2/ticker/%d/';
    protected $coinhistorySettingsFile = '/../data/coinhistory-settings.json';
    protected $updatableIds;
    protected $delay=0;

    public function __construct() {
        $this->getUpdatableIds();
    }

    public function parseDelay($args)
    {
        if(count($args)>1 && is_numeric($args[1]))
        {
            $this->delay = $args[1];
        }
    }
    /**
     * Parse arguments from command line or get array with 3 elements
     * @param array $args
     * @throws InvalidArgumentException
     */
    public function parseArgs(array $args) {
        if (count($args) < 3) {
            throw new InvalidArgumentException("Invalid arguments");
        }
        if (!is_numeric($args[1]) || !is_numeric($args[2])) {
            throw new InvalidArgumentException("arguments must be numeric from and to");
        }
        $this->from = abs($args[1]);
        $this->to = abs($args[2]);
    }

    /**
     * get correct url based on arguments
     * @return string
     */
    public function getUrl($from, $to): string {
        return sprintf($this->url, $from, $to);
    }

    /**
     * 
     * @param type $id
     * @return string
     */
    public function geUrlforById($id): string {
        return sprintf($this->byIdurl, $id);
    }

    /**
     * 
     * @return Array - list of updatable IDS
     */
    public function getUpdatableIds() {
        if (!is_array($this->updatableIds)) {
            $this->updatableIds = Helper::checkFileAndReturnObject(__DIR__ . $this->coinhistorySettingsFile)->coins_to_save;
            asort($this->updatableIds);
        }
        return $this->updatableIds;
    }

    

    /**
     * checks if id is in list from json file
     * @param type $id
     * @return type
     */
    public function isAllowed($id) {
        return in_array($id, $this->updatableIds);
    }

    /**
     * return maximum crypto currency count
     * @return int
     */
    public function getCryptoCount() {
        $data = Helper::getData($this->getUrl(1, 1),$this->delay);
        return $data->metadata->num_cryptocurrencies;
    }

    /**
     * This function get maximum currency count
     * and generate URLs with range of 100 id
     * @return Array - list of urls
     */
    public function returnUrls() {
        $count = $this->getCryptoCount();
        $limit = 100;
        $urls = [];

        $currents = [];
        for ($i = 1; $i <= $count; $i++) {
            $ccount = floor($i / 100) * 100;
            if ($ccount == 0) {
                $ccount = 1;
            }
            $currents[$ccount][] = $i;
        }
        foreach ($currents as $key => $value) {
            if ($key >= 100) {
                $key += 1;
            }
            $urls[] = $this->getUrl($key, $limit);
        }

        return $urls;
    }
    
    public  function getDelay()
    {
        return $this->delay;
    }

}
