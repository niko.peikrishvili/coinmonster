<?php

namespace CoinMonster;

use \PDO;
use \Exception;

/**
 * Description of DataBase
 *
 * @author niko.peikrishvili
 */
class DataBase
{

    protected $db;
    protected $allIds = [];
    public $source = 'CoinMarketCap';
    protected $databaseSettingsFile = '/../data/database-settings.json';
    protected $settings;

    public function __construct()
    {
        try
        {
            $this->settings = $this->getDatabaseSettings();
            $this->db = new PDO("mysql:host=".$this->settings->host.";dbname=".$this->settings->database, $this->settings->username, $this->settings->password);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e)
        {
            throw new Exception("Unable to establish connection with mysql");
        }
        
    }
    
    /**
     * get database settings from json file
     * @return type
     */
    public function getDatabaseSettings() {
        return Helper::checkFileAndReturnObject(__DIR__ . $this->databaseSettingsFile);
    }

    public function getAllIds()
    {
        $sql = "select ID from coinmaster";
        $sth = $this->db->query($sql);
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach ($rows as $row)
        {
            $this->allIds[] = $row['ID'];
        }
    }

    public function getAll()
    {
        $sql = "select * from coinmaster";
        $sth = $this->db->query($sql);
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function addToCoinMaster(\stdClass $item)
    {
        try
        {


            $sql = "INSERT INTO `coinmaster`("
                    . "`Source`, `ID`, `Name`, `Symbol`, `Website_Slug`, `Rank`, `Circulating_Supply`, `Total_Supply`, `Max_Supply`, `Last_Price`, `Volume_24h`, `Market_Cap`, `Percent_Change_1h`, `Percent_Change_24h`, `Percent_Change_7d`,`Last_Updated_On`)"
                    . " VALUES "
                    . "(:Source, :ID, :Name, :Symbol, :Website_Slug, :Rank, :Circulating_Supply, :Total_Supply, :Max_Supply, :Last_Price, :Volume_24h, :Market_Cap, :Percent_Change_1h, :Percent_Change_24h, :Percent_Change_7d,:Last_Updated_On)";

            $sth = $this->db->prepare($sql);
            $sth->bindParam(':Source', $this->source);
            $sth->bindParam(':ID', $item->id);
            $sth->bindParam(':Name', $item->name);
            $sth->bindParam(':Symbol', $item->symbol);
            $sth->bindParam(':Website_Slug', $item->website_slug);
            $sth->bindParam(':Rank', $item->rank);
            $sth->bindParam(':Circulating_Supply', $item->circulating_supply);
            $sth->bindParam(':Total_Supply', $item->total_supply);
            $sth->bindParam(':Max_Supply', $item->max_supply);
            $sth->bindParam(':Last_Price', $item->quotes->USD->price);
            $sth->bindParam(':Volume_24h', $item->quotes->USD->volume_24h);
            $sth->bindParam(':Market_Cap', $item->quotes->USD->market_cap);
            $sth->bindParam(':Percent_Change_1h', $item->quotes->USD->percent_change_1h);
            $sth->bindParam(':Percent_Change_24h', $item->quotes->USD->percent_change_24h);
            $sth->bindParam(':Percent_Change_7d', $item->quotes->USD->percent_change_7d);
            $sth->bindValue(':Last_Updated_On', date('Y-m-d H:i:s', $item->last_updated));
            $sth->execute();
        } catch (Exception $ex)
        {
            echo $ex->getMessage();
        }
    }

    public function updateCoinMaster(\stdClass $item)
    {
        try
        {


            $sql = "UPDATE `coinmaster` SET `Source`=:Source,`Name`=:Name,`Symbol`=:Symbol,`Website_Slug`=:Website_Slug,`Rank`=:Rank,`Circulating_Supply`=:Circulating_Supply,`Total_Supply`=:Total_Supply,`Max_Supply`=:Max_Supply,`Last_Price`=:Last_Price,`Volume_24h`=:Volume_24h,`Market_Cap`=:Market_Cap,`Percent_Change_1h`=:Percent_Change_1h,`Percent_Change_24h`=:Percent_Change_24h,`Percent_Change_7d`=:Percent_Change_7d,`Last_Updated_On`=:Last_Updated_On  WHERE `ID`=:ID";

            $sth = $this->db->prepare($sql);
            $sth->bindParam(':Source', $this->source);
            $sth->bindParam(':ID', $item->id);
            $sth->bindParam(':Name', $item->name);
            $sth->bindParam(':Symbol', $item->symbol);
            $sth->bindParam(':Website_Slug', $item->website_slug);
            $sth->bindParam(':Rank', $item->rank);
            $sth->bindParam(':Circulating_Supply', $item->circulating_supply);
            $sth->bindParam(':Total_Supply', $item->total_supply);
            $sth->bindParam(':Max_Supply', $item->max_supply);
            $sth->bindParam(':Last_Price', $item->quotes->USD->price);
            $sth->bindParam(':Volume_24h', $item->quotes->USD->volume_24h);
            $sth->bindParam(':Market_Cap', $item->quotes->USD->market_cap);
            $sth->bindParam(':Percent_Change_1h', $item->quotes->USD->percent_change_1h);
            $sth->bindParam(':Percent_Change_24h', $item->quotes->USD->percent_change_24h);
            $sth->bindParam(':Percent_Change_7d', $item->quotes->USD->percent_change_7d);
            $sth->bindValue(':Last_Updated_On', date('Y-m-d H:i:s', $item->last_updated));

            $sth->execute();
        } catch (Exception $ex)
        {
            echo $ex->getMessage();
        }
    }

    public function addToHistory(\stdClass $item)
    {
        try
        {
            $sql = "INSERT INTO `coinhistory`( `Source`, `ID`, `Name`, `Symbol`, `Price`, `Last_Updated_On`) VALUES (:Source,:ID,:Name,:Symbol,:Price,:Last_Updated_On)";
            $sth = $this->db->prepare($sql);
            $sth->bindParam(':Source', $this->source);
            $sth->bindParam(':ID', $item->id);
            $sth->bindParam(':Name', $item->name);
            $sth->bindParam(':Symbol', $item->symbol);
            $sth->bindParam(':Price', $item->quotes->USD->price);
            $sth->bindValue(':Last_Updated_On', date('Y-m-d H:i:s', $item->last_updated));
            $sth->execute();
        } catch (Exception $ex)
        {
            echo $ex->getMessage();
        }
    }

    public function needToInsert($id)
    {
        return !in_array($id, $this->allIds);
    }

    public function updateScrapperInfo($id, $result)
    {
        try
        {

            $sql = "UPDATE `coinmaster` SET "
                    . "`Description`=:Description,"
                    . "`Website`=:Website,"
                    . "`Icon`=:Icon,"
                    . "`Logo`=:Logo,"
                    . "`Image`=:Image,"
                    . "`SourceCode`=:SourceCode,"
                    . "`Twitter`=:Twitter,"
                    . "`Explorer`=:Explorer WHERE ID=:ID";

            $sth = $this->db->prepare($sql);
            $sth->bindParam(':Description', $result['description']);
            $sth->bindParam(':Website', $result['website']);
            $sth->bindParam(':Icon', $result['icon']);
            $sth->bindParam(':Logo', $result['logo']);
            $sth->bindParam(':Image', $result['image']);
            $sth->bindParam(':SourceCode', $result['source code']);
            $sth->bindParam(':Twitter', $result['twitter']);
            $sth->bindParam(':Explorer', $result['explorer']);
            $sth->bindParam(':ID', $id);
            $sth->execute();
        } catch (Exception $ex)
        {
            echo $ex->getMessage();
        }
    }

    public function get24HourMinMaxForCrypto($id)
    {
        $sql = "SELECT min(Price) as low,max(Price) as high FROM coinhistory where ID=:id and Last_Updated_On between date_sub(now(),INTERVAL 24 HOUR) and now()";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':id', $id);
        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    public function update24HourMinMax($id, $min, $max)
    {
        $sql = "UPDATE coinmaster set `24h_High`=:high,`24h_Low`=:min where ID=:id";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':high', $max);
        $sth->bindParam(':min', $min);
        $sth->bindParam(':id', $id);
        $sth->execute();
    }

    /**
     * IApiTickerItem {
      exchmktId (integer, optional),
      exchName (string, optional),
      exchCode (string, optional),
      marketName (string, optional),
      displayName (string, optional),
      last (number, optional),
      high (number, optional),
      low (number, optional),
      volume (number, optional),
      baseCurrCode (string, optional),
      quoteCurrCode (string, optional),
      baseIsFiat (boolean, optional),
      quoteIsFiat (boolean, optional)
      }


     * @param type $item
     */
    public function insertCoinigyHistory($item)
    {
        try
        {

            $sql = "INSERT INTO `coinigyhistory`
(`exchmktId`, `exchName`, `exchCode`, `marketName`, `displayName`, `last`, `high`, `low`, `volume`, `baseCurrCode`, `quoteCurrCode`, `baseIsFiat`, `quoteIsFiat`, `change`, `changepercent`, `1hlow`, `1hhigh`, `24hlow`, `24hhigh`, `7dlow`, `7dhigh`,`created_at`) 
VALUES (:exchmktId,:exchName,:exchCode,:marketName,:displayName,:last,:high,:low,:volume,:baseCurrCode,:quoteCurrCode,:baseIsFiat,:quoteIsFiat,:change,:changepercent,:1hlow,:1hhigh,:24hlow,:24hhigh,:7dlow,:7dhigh,now())";
            $sth = $this->db->prepare($sql);
            $sth->bindParam(':exchmktId', $item->exchmktId);
            $sth->bindParam(':exchName', $item->exchName);
            $sth->bindParam(':exchCode', $item->exchCode);
            $sth->bindParam(':marketName', $item->marketName);
            $sth->bindParam(':displayName', $item->displayName);
            $sth->bindParam(':last', $item->last);
            $sth->bindParam(':high', $item->high);
            $sth->bindParam(':low', $item->low);
            $sth->bindParam(':volume', $item->volume);
            $sth->bindParam(':baseCurrCode', $item->baseCurrCode);
            $sth->bindParam(':quoteCurrCode', $item->quoteCurrCode);
            $sth->bindParam(':quoteIsFiat', $item->quoteIsFiat);
            $sth->bindParam(':change', $item->change);
            $sth->bindParam(':changepercent', $item->changepercent);
            $sth->bindParam(':1hlow', $item->oneHLow);
            $sth->bindParam(':1hhigh', $item->oneHHigh);
            $sth->bindParam(':24hhigh', $item->twentyFourHHigh);
            $sth->bindParam(':24low', $item->twentyFourHLow);
            $sth->bindParam(':7dlow', $item->sevenDayLow);
            $sth->bindParam(':7dhigh', $item->sevenDayHigh);
            $sth->execute();
        } catch (Exception $ex)
        {
            throw new Exception("Unable to write to coinigy table: " . $ex->getMessage());
        }
    }

    protected function calculateChange($original, $current)
    {
        $diff = abs(($current - $original));
        return ($diff / $original) * 100;
    }

    public function getCoinigyChanges($item)
    {
        $sql = "select * from coinigyhistory where baseCurrCode=:baseCurrCode and quoteCurrCode=:quoteCurrCode order by coinigyid desc limit 1";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':baseCurrCode', $item->baseCurrCode);
        $sth->bindParam(':quoteCurrCode', $item->quoteCurrCode);
        $sth->execute();
        $result = new \stdClass();
        $result->change = 0;
        $result->changepercent = 0;
        if ($sth->rowCount() == 1)
        {
            $row = $sth->fetch(PDO::FETCH_ASSOC);
            $result->change = $item->last - $row['last'];
            $result->changepercent = $this->calculateChange($item->last, $row['last']);
        }
        return $result;
    }
    public function getMinMaxForCoinigyByHours($item,$hours)
    {
        $sql = "SELECT min(last) as low,max(last) as high FROM coinigyhistory where baseCurrCode=:baseCurrCode and quoteCurrCode=:quoteCurrCode and  created between date_sub(now(),INTERVAL ".$hours." HOUR) and now()";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':baseCurrCode', $item->baseCurrCode);
        $sth->bindParam(':quoteCurrCode', $item->quoteCurrCode);
        $sth->execute();
        $result = new \stdClass();
        $result->low = 0;
        $result->high = 0;
        if ($sth->rowCount() == 1)
        {
            $row = $sth->fetch(PDO::FETCH_ASSOC);
            $result->low = $row['low'];
            $result->high = $row['high'];
        }
        return $result;
    }

}
